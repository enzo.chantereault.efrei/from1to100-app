const printAssociatedTextFrom1To100 = (dupletRules) => {
    for (let number = 0; number <= 100; number++) {

        dupletRules.forEach(dupletRule => {

            if (isExactMultipleOrContains(number, dupletRule.number)) {
                displayResult(number, dupletRule.text)
            }

        });

    }
}

const isExactMultipleOrContains = (numberToCheck, numberToCompare) => {
    var stringToCheck = `${numberToCheck}`;
    var stringToCompare = `${numberToCompare}`;

    if (numberToCheck % numberToCompare != 0 && !stringToCheck.includes(stringToCompare)) {
        return false;
    }

    return true;
}

const displayResult = (checkedNumber, text) => {
    console.log(`${checkedNumber} -> ${text}`);
}

var dupletRules = [
    {
        number: 3,
        text: 'Fizz'
    },
    {
        number: 5,
        text: 'Buzz'
    }
];

printAssociatedTextFrom1To100(dupletRules);